package com.adriancrisan.expensetracker.data.dao

import androidx.room.*
import com.adriancrisan.expensetracker.data.entities.Label

@Dao
interface LabelDAO {
    @Query("SELECT * FROM labels ORDER BY name")
    suspend fun getAll() : List<Label>

    @Query("SELECT * FROM labels WHERE labelId=:labelId LIMIT 1")
    suspend fun get(labelId: Long) : Label

    @Insert
    suspend fun insertAll(vararg label: Label): List<Long>

    @Update
    suspend fun updateLabel(vararg label: Label): Int

    @Delete
    suspend fun delete(label: Label)
}