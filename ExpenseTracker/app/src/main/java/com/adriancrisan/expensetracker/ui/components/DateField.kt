package com.adriancrisan.expensetracker.ui.components

import android.util.Log
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.*
import java.lang.Integer.max
import java.time.OffsetDateTime
import java.time.YearMonth
import kotlin.math.min

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DateField(
    dayOfMonth: Int,
    month: Int,
    year: Int,
    onValueChange: (Int, Int, Int) -> Unit,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    readOnly: Boolean = false,
    textStyle: TextStyle = LocalTextStyle.current,
    label: @Composable (() -> Unit)? = null,
    placeholder: @Composable (() -> Unit)? = null,
    leadingIcon: @Composable (() -> Unit)? = null,
    trailingIcon: @Composable (() -> Unit)? = null,
    supportingText: @Composable (() -> Unit)? = null,
    isError: Boolean = false,
    keyboardActions: KeyboardActions = KeyboardActions.Default,
    singleLine: Boolean = false,
    maxLines: Int = if (singleLine) 1 else Int.MAX_VALUE,
    minLines: Int = 1,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    type: TextFieldType = TextFieldType.Outlined,
    shape: Shape = if (type == TextFieldType.Outlined) TextFieldDefaults.outlinedShape else TextFieldDefaults.filledShape,
    colors: TextFieldColors = if (type == TextFieldType.Outlined) TextFieldDefaults.outlinedTextFieldColors() else TextFieldDefaults.textFieldColors(),
    fixCursorAtTheEnd: Boolean = true,
    separator: Char = '/',
) {
    var newDay by remember { mutableStateOf(dayOfMonth) }
    var newMonth by remember { mutableStateOf(month) }
    var newYear by remember { mutableStateOf(year) }


    val formattedDay = if (newDay in 0..9)
        "0$dayOfMonth"
    else
        "$dayOfMonth"

    val formattedMonth = if (newMonth in 0..9)
        "0$month"
    else
        "$month"
    val formattedYear = "$newYear"

    val value = "$formattedDay$formattedMonth$formattedYear"

    var cursor by remember { mutableStateOf(0) }
    val textValue = TextFieldValue(text = value, selection = TextRange(cursor))

    val dateVisualTransformation = DateVisualTransformation(
        fixCursorAtTheEnd = fixCursorAtTheEnd,
        separator = separator,
        cursorPos = cursor,
    )

    val onTextValueChange: (TextFieldValue) -> Unit = { newTextValue ->
        if (newTextValue.text != value) {
            val it = newTextValue.text
            if (it.length > 8) {
                if (++cursor > 8) cursor = 0
                val adjustedString: String
                if (cursor != 0) {
                    adjustedString =
                        buildString {
                            append(
                                it.slice(
                                    IntRange(0, cursor - 1)
                                )
                            )
                            append(
                                it.slice(
                                    IntRange(cursor + 1, it.length - 1)
                                )
                            )
                        }
                } else {
                    adjustedString = it.take(8)
                }

                val currentDate = OffsetDateTime.now()
                newYear = min(max(adjustedString.takeLast(4).toInt(), 1999), currentDate.year)
                newMonth = min(max(adjustedString.slice(IntRange(2, 3)).toInt(), 1), 12)
                newDay = min(max(adjustedString.take(2).toInt(), 1), 31)
            } else {
                if (cursor != 0) {
                    val adjustedString = buildString {
                        if (cursor - 1 > 0) {
                            append(
                                it.slice(IntRange(0, cursor - 2))
                            )
                        }
                        append(
                            "0"
                        )
                        if (it.length - cursor + 1 > 0) {
                            append(it.slice(IntRange(cursor - 1, it.length - 1)))
                        }
                    }

                    val currentDate = OffsetDateTime.now()
                    newYear = min(max(adjustedString.takeLast(4).toInt(), 1999), currentDate.year)
                    newMonth = min(max(adjustedString.slice(IntRange(2, 3)).toInt(), 1), 12)
                    newDay = min(max(adjustedString.take(2).toInt(), 1), 31)
                } else {
                    newDay = dayOfMonth
                    newMonth = month
                    newYear = year
                }
                if (--cursor < 0) cursor = 8
            }

            onValueChange(newDay, newMonth, newYear)
        } else if (newTextValue.selection.start != cursor) {
            cursor = when {
                newTextValue.selection.start <= 8 -> newTextValue.selection.start
                else -> 8
            }
        }
    }

    when (type) {
        TextFieldType.Filled -> TextField(
            value = textValue,
            onValueChange = onTextValueChange,
            modifier = modifier,
            enabled = enabled,
            readOnly = readOnly,
            textStyle = textStyle,
            label = label,
            placeholder = placeholder,
            leadingIcon = leadingIcon,
            trailingIcon = trailingIcon,
            supportingText = supportingText,
            isError = isError,
            keyboardActions = keyboardActions,
            singleLine = singleLine,
            maxLines = maxLines,
            minLines = minLines,
            interactionSource = interactionSource,
            shape = shape,
            colors = colors,
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.NumberPassword,
                imeAction = ImeAction.Done
            ),
            visualTransformation = dateVisualTransformation,
        )
        TextFieldType.Outlined -> OutlinedTextField(
            value = textValue,
            onValueChange = onTextValueChange,
            modifier = modifier,
            enabled = enabled,
            readOnly = readOnly,
            textStyle = textStyle,
            label = label,
            placeholder = placeholder,
            leadingIcon = leadingIcon,
            trailingIcon = trailingIcon,
            supportingText = supportingText,
            isError = isError,
            keyboardActions = keyboardActions,
            singleLine = singleLine,
            maxLines = maxLines,
            minLines = minLines,
            interactionSource = interactionSource,
            shape = shape,
            colors = colors,
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.NumberPassword,
                imeAction = ImeAction.Done
            ),
            visualTransformation = dateVisualTransformation,
        )
    }
}


class DateVisualTransformation(
    private val fixCursorAtTheEnd: Boolean,
    private val separator: Char,
    private val cursorPos: Int
) : VisualTransformation {
    override fun filter(text: AnnotatedString): TransformedText {
        val inputText = text.text

        val dayPart = inputText.take(2)
        val monthPart = inputText.slice(IntRange(2, 3))
        val yearPart = inputText.takeLast(4)

        val formatterDate = dayPart + separator + monthPart + separator + yearPart

        val newText = AnnotatedString(
            text = formatterDate,
            spanStyles = text.spanStyles,
            paragraphStyles = text.paragraphStyles
        )
        val offsetMapping =
            if (fixCursorAtTheEnd)
                DateFixedCursorMapping(
                    cursorPos = cursorPos,
                )
            else
                DateMovableCursorMapping()

        return TransformedText(newText, offsetMapping)
    }
}

class DateFixedCursorMapping(
    private val cursorPos: Int,
) : OffsetMapping {
    override fun originalToTransformed(offset: Int): Int {
        return when {
            cursorPos < 2 -> cursorPos
            cursorPos < 4 -> cursorPos + 1
            else -> cursorPos + 2
        }
    }

    override fun transformedToOriginal(offset: Int): Int =
        cursorPos
}

class DateMovableCursorMapping : OffsetMapping {
    override fun originalToTransformed(offset: Int): Int {
        val newOff = when (offset) {
            0, 1 -> offset
            2, 3 -> offset + 1
            4, 5, 6, 7, 8, 9 -> offset + 2
            else -> 0
        }
        return newOff
    }

    override fun transformedToOriginal(offset: Int): Int {
        val newOff = when (offset) {
            0, 1 -> offset
            2, 3 -> offset - 1
            4, 5, 6, 7, 8, 9 -> offset - 2
            else -> 0
        }
        return newOff
    }
}