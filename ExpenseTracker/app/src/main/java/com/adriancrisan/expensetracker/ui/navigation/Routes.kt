package com.adriancrisan.expensetracker.ui.navigation

enum class Routes {
    Overview,
    Monthly,
    Budget,
    Settings,
    Profile,
    NewExpense,
    NewExpenseScan,
    EditExpense,
    NewLabel,
    DetailLabel,
    EditLabel
    ;

}