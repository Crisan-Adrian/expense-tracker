package com.adriancrisan.expensetracker.ui.icons

import android.util.Log
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Abc
import androidx.compose.material.icons.filled.Paid
import androidx.compose.material.icons.filled.Pets
import androidx.compose.material.icons.filled.ShoppingBag
import androidx.compose.ui.graphics.vector.ImageVector
import com.adriancrisan.expensetracker.R
import com.adriancrisan.expensetracker.data.entities.LabelIcon
import com.adriancrisan.expensetracker.data.entities.LabelIconType

enum class MyVectorIcons(val imageVector: ImageVector) {
    ShoppingBag(Icons.Filled.ShoppingBag),
    Abc(Icons.Filled.Abc),
    Paid(Icons.Filled.Paid),
    Pets(Icons.Filled.Pets),

    ;

    companion object {
        fun getIcon(value: String): ImageVector {
            val name = value.split(".").last()
            return valueOf(name).imageVector
        }
    }
}

enum class MyPainterIcons(val painterResource: Int) {
    IcBarcodeScanner(R.drawable.ic_barcode_scanner),
    ;

    companion object {
        fun getIcon(value: String): Int {
            val name = value.split("_").joinToString("")
            return valueOf(name).painterResource
        }
    }
}

object MyIcons {
    var icons: MutableList<LabelIcon>? = null

    fun getIconList(): List<LabelIcon> {
        if(icons != null) {
            return icons!!
        }

        icons = mutableListOf()
        for (e in MyVectorIcons.values()) {
            val icon = LabelIcon(e.name, LabelIconType.VECTOR)
            icons!!.add(icon)
        }

        for (e in MyPainterIcons.values()) {
            val icon = LabelIcon(e.name, LabelIconType.PAINTER)
            icons!!.add(icon)
        }

        return icons!!
    }
}