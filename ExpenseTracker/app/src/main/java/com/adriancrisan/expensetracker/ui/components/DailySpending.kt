package com.adriancrisan.expensetracker.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.adriancrisan.expensetracker.data.entities.Expense
import kotlin.math.roundToInt

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DailySpending(expenses:List<Expense>) {
    val spentToday: Int = expenses.fold(0f) { acc, next -> acc + next.value }.roundToInt()

    Card(onClick = {}) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(all = 8.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text("Daily Spending")
            Text(text = "$spentToday RON", fontSize = 24.sp)
            TopLabels(expenses)
        }
    }
}