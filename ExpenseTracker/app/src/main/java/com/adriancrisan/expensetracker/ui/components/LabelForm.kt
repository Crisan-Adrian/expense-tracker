package com.adriancrisan.expensetracker.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.adriancrisan.expensetracker.data.entities.Label
import com.adriancrisan.expensetracker.data.entities.LabelIcon


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LabelForm(
    label: Label,
    title: @Composable () -> Unit,
    actionButtonContent: @Composable () -> Unit,
    icons: List<LabelIcon>,
    onSubmit: (Label) -> Unit,
    onCancel: () -> Unit,
) {
    var labelName by remember { mutableStateOf(label.name) }
    var labelIcon: LabelIcon by remember { mutableStateOf(label.icon) }

    Scaffold(
        modifier = Modifier.fillMaxSize(),
        topBar = {
            CenterAlignedTopAppBar(
                title = { title() },
                navigationIcon = {
                    IconButton(onClick = {
                        onCancel()
                    }) {
                        Icon(Icons.Filled.Close, contentDescription = null)
                    }
                },
                actions = {
                    TextButton(
                        onClick = {
                            val newLabel = label.copy(name = labelName, icon = labelIcon)
                            onSubmit(newLabel)
                        }) {
                        actionButtonContent()
                    }
                }
            )
        }
    ) { paddingValues ->
        Column(
            modifier = Modifier
                .padding(paddingValues)
                .padding(8.dp)
                .fillMaxWidth(),
            verticalArrangement = Arrangement.spacedBy(16.dp)
        ) {
            OutlinedTextField(
                value = labelName,
                onValueChange = { labelName = it },
                modifier = Modifier.fillMaxWidth()
            )
            IconPicker(
                icons = icons,
                selected = labelIcon,
                onSelectionChange = { icon ->
                    labelIcon = icon
                },
                modifier = Modifier.fillMaxWidth()
            )
        }
    }
}