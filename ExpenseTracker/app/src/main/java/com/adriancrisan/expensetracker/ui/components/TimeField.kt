package com.adriancrisan.expensetracker.ui.components

import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.*
import kotlin.math.min

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TimeField(
    hour: Int,
    minute: Int,
    onValueChange: (Int, Int) -> Unit,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    readOnly: Boolean = false,
    textStyle: TextStyle = LocalTextStyle.current,
    label: @Composable (() -> Unit)? = null,
    placeholder: @Composable (() -> Unit)? = null,
    leadingIcon: @Composable (() -> Unit)? = null,
    trailingIcon: @Composable (() -> Unit)? = null,
    supportingText: @Composable (() -> Unit)? = null,
    isError: Boolean = false,
    keyboardActions: KeyboardActions = KeyboardActions.Default,
    singleLine: Boolean = false,
    maxLines: Int = if (singleLine) 1 else Int.MAX_VALUE,
    minLines: Int = 1,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    type: TextFieldType = TextFieldType.Outlined,
    shape: Shape = if (type == TextFieldType.Outlined) TextFieldDefaults.outlinedShape else TextFieldDefaults.filledShape,
    colors: TextFieldColors = if (type == TextFieldType.Outlined) TextFieldDefaults.outlinedTextFieldColors() else TextFieldDefaults.textFieldColors(),
    fixCursorAtTheEnd: Boolean = true,
    separator: Char = ':',
) {

    val formattedHour = if (hour in 0..9)
        "0$hour"
    else
        "$hour"

    val formattedMinute = if (minute in 0..9)
        "0$minute"
    else
        "$minute"

    val value = "$formattedHour$formattedMinute"

    var cursor by remember { mutableStateOf(0) }
    val textValue = TextFieldValue(text = value, selection = TextRange(cursor))

    val timeVisualTransformation = TimeVisualTransformation(
        fixCursorAtTheEnd = fixCursorAtTheEnd,
        separator = separator,
        cursorPos = cursor
    )

    val valueChange: (TextFieldValue) -> Unit = { newTextValue ->
        if (newTextValue.text != value) {
            val it = newTextValue.text
            val newHour: Int
            val newMinute: Int
            if (it.length > 4) {
                if (++cursor > 4) cursor = 0
                val adjustedString: String
                if (cursor != 0) {
                    adjustedString =
                        buildString {
                            append(
                                it.slice(
                                    IntRange(0, cursor - 1)
                                )
                            )
                            append(
                                it.slice(
                                    IntRange(cursor + 1, it.length - 1)
                                )
                            )
                        }
                } else {
                    adjustedString = it.take(4)
                }
                newHour = min(adjustedString.take(2).toInt(), 23)
                newMinute = min(adjustedString.takeLast(2).toInt(), 59)
            } else {
                if (cursor != 0) {
                    val adjustedString = buildString {
                        if (cursor - 1 > 0) {
                            append(
                                it.slice(IntRange(0, cursor - 2))
                            )
                        }
                        append(
                            "0"
                        )
                        if (it.length - cursor + 1 > 0) {
                            append(it.slice(IntRange(cursor - 1, it.length - 1)))
                        }
                    }
                    newHour = min(adjustedString.take(2).toInt(), 23)
                    newMinute = min(adjustedString.takeLast(2).toInt(), 59)
                } else {
                    newHour = hour
                    newMinute = minute
                }
                if (--cursor < 0) cursor = 4
            }
            onValueChange(newHour, newMinute)
        } else if (newTextValue.selection.start != cursor) {
            cursor = when {
                newTextValue.selection.start <= 4 -> newTextValue.selection.start
                else -> 4
            }
        }
    }

    when (type) {
        TextFieldType.Filled -> TextField(
            value = textValue,
            onValueChange = valueChange,
            modifier = modifier,
            enabled = enabled,
            readOnly = readOnly,
            textStyle = textStyle,
            label = label,
            placeholder = placeholder,
            leadingIcon = leadingIcon,
            trailingIcon = trailingIcon,
            supportingText = supportingText,
            isError = isError,
            keyboardActions = keyboardActions,
            singleLine = singleLine,
            maxLines = maxLines,
            minLines = minLines,
            interactionSource = interactionSource,
            shape = shape,
            colors = colors,
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.NumberPassword,
                imeAction = ImeAction.Done
            ),
            visualTransformation = timeVisualTransformation,
        )
        TextFieldType.Outlined -> OutlinedTextField(
            value = textValue,
            onValueChange = valueChange,
            modifier = modifier,
            enabled = enabled,
            readOnly = readOnly,
            textStyle = textStyle,
            label = label,
            placeholder = placeholder,
            leadingIcon = leadingIcon,
            trailingIcon = trailingIcon,
            supportingText = supportingText,
            isError = isError,
            keyboardActions = keyboardActions,
            singleLine = singleLine,
            maxLines = maxLines,
            minLines = minLines,
            interactionSource = interactionSource,
            shape = shape,
            colors = colors,
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.NumberPassword,
                imeAction = ImeAction.Done
            ),
            visualTransformation = timeVisualTransformation,
        )
    }
}

class TimeVisualTransformation(
    private val fixCursorAtTheEnd: Boolean,
    private val separator: Char,
    private val cursorPos: Int
) : VisualTransformation {
    override fun filter(text: AnnotatedString): TransformedText {
        val inputText = text.text

        val hourPart = inputText.take(2)
        val minutePart = inputText.takeLast(2)

        val formatterDate = hourPart + separator + minutePart

        val newText = AnnotatedString(
            text = formatterDate,
            spanStyles = text.spanStyles,
            paragraphStyles = text.paragraphStyles
        )
        val offsetMapping =
            if (fixCursorAtTheEnd)
                TimeFixedCursorMapping(
                    cursorPos = cursorPos,
                )
            else
                TimeMovableCursorMapping()

        return TransformedText(newText, offsetMapping)
    }
}

class TimeFixedCursorMapping(
    private val cursorPos: Int,
) : OffsetMapping {
    override fun originalToTransformed(offset: Int): Int =
        when {
            cursorPos < 2 -> cursorPos
            else -> cursorPos + 1
        }

    override fun transformedToOriginal(offset: Int): Int =
        when {
            else -> cursorPos
        }
}

class TimeMovableCursorMapping : OffsetMapping {
    override fun originalToTransformed(offset: Int): Int {
        val newOff = when (offset) {
            0, 1 -> offset
            2, 3, 4 -> offset + 1
            else -> 4
        }
        return newOff
    }

    override fun transformedToOriginal(offset: Int): Int {
        val newOff = when (offset) {
            0, 1 -> offset
            2, 3, 4 -> offset - 1
            else -> offset - 1
        }
        return newOff
    }
}
