package com.adriancrisan.expensetracker.ui.components

import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import com.adriancrisan.expensetracker.data.entities.LabelIcon
import com.adriancrisan.expensetracker.data.entities.LabelIconType
import com.adriancrisan.expensetracker.ui.icons.MyPainterIcons
import com.adriancrisan.expensetracker.ui.icons.MyVectorIcons


@Composable
fun LabelIcon(
    labelIcon: LabelIcon,
    modifier: Modifier = Modifier,
) {
    when (labelIcon.type) {
        LabelIconType.VECTOR -> {
            val resource =
                MyVectorIcons.getIcon(labelIcon.icon)
            Icon(
                resource,
                contentDescription = null,
                modifier = modifier
            )
        }
        LabelIconType.PAINTER -> {
            val resource = painterResource(id =
            MyPainterIcons.valueOf(labelIcon.icon).painterResource)
            Icon(
                resource,
                contentDescription = null,
                modifier = modifier
            )
        }
    }
}