package com.adriancrisan.expensetracker.data.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.SET_NULL
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.adriancrisan.expensetracker.utils.DateTimeConverter
import java.time.OffsetDateTime
import java.time.ZoneOffset

@Entity(
    tableName = "expenses"
)
data class Expense(
    @PrimaryKey(autoGenerate = true)
    val expenseId: Long? = null,
    val name: String,
    val value: Float,
    @TypeConverters(DateTimeConverter::class)
    val date: OffsetDateTime,
) {
    companion object {
        val Default: Expense = Expense(name = "",
            value = 0f,
            date = OffsetDateTime.of(
                2000, 1, 1,
                1, 1, 1, 1,
                ZoneOffset.UTC))
    }
}


