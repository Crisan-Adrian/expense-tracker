package com.adriancrisan.expensetracker.ui.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ChevronLeft
import androidx.compose.material.icons.filled.ChevronRight
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp
import java.time.YearMonth

@Composable
fun MonthPicker(yearMonth: YearMonth, onChange: (YearMonth) -> Unit) {
    Row(
        modifier = Modifier
            .fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center
    ) {
        TextButton(onClick = { onChange(yearMonth.minusMonths(1)) }) {
            Icon(Icons.Default.ChevronLeft, contentDescription = "previous month")
        }
        Text(
            text = "${yearMonth.month} ${yearMonth.year}",
            fontSize = 20.sp,
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth(.6f)
        )
        TextButton(onClick = { onChange(yearMonth.plusMonths(1)) }) {
            Icon(Icons.Default.ChevronRight, contentDescription = "next month")
        }
    }
}