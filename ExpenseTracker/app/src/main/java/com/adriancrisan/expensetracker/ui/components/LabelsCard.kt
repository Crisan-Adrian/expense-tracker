package com.adriancrisan.expensetracker.ui.components

import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.adriancrisan.expensetracker.data.entities.Label
import com.adriancrisan.expensetracker.ui.navigation.Routes

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LabelsCard(
    navController: NavController,
    labels: List<Label>,
    rowSize: Int = 3,
) {
    val scrollState = rememberScrollState()

    Column(
        verticalArrangement = Arrangement.spacedBy(8.dp),
    ) {
        Text(
            text = "Your Labels",
            fontSize = 24.sp,
            color = MaterialTheme.colorScheme.secondary,
            modifier = Modifier.padding(24.dp, 8.dp),
        )
        MyHorizontalGrid(gridRows = MyGridRows.FixedRows(rowSize),
            modifier = Modifier.horizontalScroll(scrollState),
            contentPadding = PaddingValues(16.dp, 0.dp)) {

            AssistChip(onClick = { navController.navigate(Routes.NewLabel.name) },
                label = { Text("New Label") },
                leadingIcon = { Icon(Icons.Filled.Add, contentDescription = "Add new label") },
                colors = AssistChipDefaults.assistChipColors(leadingIconContentColor = MaterialTheme.colorScheme.secondary))

            labels.forEach { label ->
                AssistChip(onClick = { navController.navigate("${Routes.EditLabel.name}/${label.labelId}") },
                    label = { Text(label.name) },
                    leadingIcon = {
                        LabelIcon(labelIcon = label.icon)
                    },
                    colors = AssistChipDefaults.assistChipColors(leadingIconContentColor = MaterialTheme.colorScheme.secondary))
            }
        }
    }
}
