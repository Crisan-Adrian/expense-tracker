package com.adriancrisan.expensetracker.ui.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.adriancrisan.expensetracker.data.IExpenseRepository
import com.adriancrisan.expensetracker.data.entities.Expense
import com.adriancrisan.expensetracker.data.entities.ExpenseWithLabels
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.time.LocalTime
import java.time.OffsetDateTime
import java.time.YearMonth
import javax.inject.Inject

@HiltViewModel
class ExpenseModel @Inject constructor(
    private val repo: IExpenseRepository
) : ViewModel() {
    private var _expenses: MutableLiveData<List<Expense>> = MutableLiveData()
    var expenses: LiveData<List<Expense>> = _expenses
    private val _dailyExpenses: MutableLiveData<List<Expense>> = MutableLiveData()
    val dailyExpenses: LiveData<List<Expense>> = _dailyExpenses
    private val _monthlyExpenses: MutableLiveData<List<Expense>> = MutableLiveData()
    val monthlyExpenses: LiveData<List<Expense>> = _monthlyExpenses

    private var date: OffsetDateTime = OffsetDateTime.now()
    private var month: YearMonth = YearMonth.now()

    init {
        fetchExpenses()
    }

    private fun updateFilters() {
        filterMonth()
        filterToday()
    }

    suspend fun getExpenseWithLabels(expenseId: Long) : ExpenseWithLabels {
        return repo.getExpenseWithLabels(expenseId)
    }

    suspend fun updateExpenseWithLabels(expenseWithLabels: ExpenseWithLabels) {
        val success = repo.updateExpenseWithLabels(expenseWithLabels)
        if (success) {
            val expenses = _expenses.value as MutableList
            _expenses.value = expenses.map { e ->
                if (e.expenseId != expenseWithLabels.expense.expenseId)
                    e
                else
                    expenseWithLabels.expense
            }
            viewModelScope.launch {
                updateFilters()
            }
        }
    }

    fun setMonth(yearMonth: YearMonth) {
        month = yearMonth

        viewModelScope.launch {
            filterMonth()
        }
    }

    private fun filterMonth() {
        val expenses = _expenses.value ?: listOf()

        _monthlyExpenses.value = expenses.filter { expense ->
            expense.date.month == month.month && expense.date.year == month.year
        }.toList()
    }

    fun setDay(date: OffsetDateTime) {
        this.date = date
        viewModelScope.launch {
            filterToday()
        }
    }

    private fun filterToday() {
        val expenses = _expenses.value ?: listOf()

        val dayStart = date.with(LocalTime.MIN)
        val dayEnd = date.with(LocalTime.MAX)

        _dailyExpenses.value = expenses.filter { expense ->
            expense.date.isAfter(dayStart) && expense.date.isBefore(dayEnd)
        }.toList()
    }

    private fun fetchExpenses() {
        viewModelScope.launch {
            val expenseList = repo.getExpenses()
            _expenses.value = expenseList
        }
    }

    suspend fun addExpense(expenseWithLabels: ExpenseWithLabels) {
        try {
            val id = repo.addExpense(expenseWithLabels)
            val expenseWithId = expenseWithLabels.expense.copy(expenseId = id)
            val expenses: MutableList<Expense> = _expenses.value as MutableList<Expense>
            expenses.add(0, expenseWithId)
            _expenses.value = expenses
            viewModelScope.launch {
                updateFilters()
            }
        } catch (e: Exception) {
            Log.d("Ex", "Encountered an error while adding a new expense: ${e.message}")
        }
    }
}