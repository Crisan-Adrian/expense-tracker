package com.adriancrisan.expensetracker.ui.screens

import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.navigation.NavController
import com.adriancrisan.expensetracker.data.entities.Label
import com.adriancrisan.expensetracker.data.entities.LabelIcon
import com.adriancrisan.expensetracker.ui.components.LabelForm
import com.adriancrisan.expensetracker.ui.icons.MyIcons
import com.adriancrisan.expensetracker.ui.viewmodels.LabelsModel
import kotlinx.coroutines.launch


@Composable
fun EditLabelScreen(
    navController: NavController,
    labelsModel: LabelsModel,
    labelId: Long,
) {
    var icons: List<LabelIcon> by remember { mutableStateOf(listOf()) }

    LaunchedEffect(navController) {
        icons = MyIcons.getIconList()
    }
    val coroutineScope = rememberCoroutineScope()

    var label: Label by remember { mutableStateOf(Label(name = "")) }
    var loaded by remember { mutableStateOf(false) }

    LaunchedEffect(key1 = labelId) {
        label = labelsModel.getLabel(labelId)
        loaded = true
    }

    val editLabel: suspend () -> Unit = {
        labelsModel.updateLabel(label)
    }

    if (!loaded) {
        CircularProgressIndicator()
    } else {
        LabelForm(
            label = label,
            title = { Text("Edit label") },
            actionButtonContent = { Text("Update") },
            icons = icons,
            onSubmit = { l ->
                label = l
                coroutineScope.launch {
                    editLabel()
                }
                navController.popBackStack()
            },
            onCancel = { navController.popBackStack() }
        )
    }
}
