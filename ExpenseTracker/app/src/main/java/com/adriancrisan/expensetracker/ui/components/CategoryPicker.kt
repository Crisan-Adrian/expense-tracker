package com.adriancrisan.expensetracker.ui.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import com.adriancrisan.expensetracker.data.entities.Label

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CategoryPicker(
    selected: Label?,
    categories: List<Label> = listOf(),
    onChange: (Label) -> Unit,
) {
    var expanded by remember { mutableStateOf(false) }

    ExposedDropdownMenuBox(
        expanded = expanded,
        onExpandedChange = {
            expanded = !expanded
        },
        modifier = Modifier.fillMaxWidth(.8f)
    ) {
        OutlinedTextField(
            readOnly = true,
            value = selected?.name ?: "Pick a category",
            onValueChange = { },
            label = { Text("Category") },
            trailingIcon = {
                ExposedDropdownMenuDefaults.TrailingIcon(
                    expanded = expanded
                )
            },
            colors = ExposedDropdownMenuDefaults.outlinedTextFieldColors(),
            modifier = Modifier.menuAnchor().fillMaxWidth()
        )
        ExposedDropdownMenu(
            expanded = expanded,
            onDismissRequest = {
                expanded = false
            },
        ) {
            categories.map { category ->
                DropdownMenuItem(
                    onClick = {
                        onChange(category)
                        expanded = false
                    },
                    text = {
                        Text(text = category.name)
                    }
                )
            }
        }
    }
}