package com.adriancrisan.expensetracker.data.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import com.adriancrisan.expensetracker.data.entities.Expense
import com.adriancrisan.expensetracker.data.entities.ExpenseWithLabels

@Dao
interface ExpenseDAO {
    @Query("SELECT * FROM expenses ORDER BY name")
    suspend fun getAll() : List<Expense>

    @Query("SELECT * FROM expenses")
    @Transaction
    suspend fun getAllWithLabels() : List<ExpenseWithLabels>

    @Query("SELECT * FROM expenses where expenseId=:id")
    @Transaction
    suspend fun getWithLabels(id:Long) : ExpenseWithLabels

    @Insert
    suspend fun insertAll(vararg expenseEntity: Expense): List<Long>

    @Update
    suspend fun updateExpenses(vararg expenseEntity: Expense) : Int

    @Delete
    suspend fun delete(expenseEntity: Expense)
}