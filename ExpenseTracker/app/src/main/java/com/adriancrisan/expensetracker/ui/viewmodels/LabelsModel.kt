package com.adriancrisan.expensetracker.ui.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.adriancrisan.expensetracker.data.ILabelsRepository
import com.adriancrisan.expensetracker.data.entities.Label
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject
import java.lang.Exception

@HiltViewModel
class LabelsModel @Inject constructor(
    private val labelsRepository: ILabelsRepository,
) : ViewModel() {
    private var _labels: MutableLiveData<List<Label>> = MutableLiveData()
    var labels: LiveData<List<Label>> = _labels

    init {
        fetchCategories()
    }

    private fun fetchCategories() {
        viewModelScope.launch {
            val categoriesList = labelsRepository.getAll()
            _labels.value = categoriesList
        }
    }

    suspend fun getLabel(labelId: Long) : Label {
        return labelsRepository.get(labelId)
    }

    suspend fun addLabel(label: Label) {
        try {
            val id = labelsRepository.addLabel(label)
            val labelWithId = label.copy(labelId = id)
            val categoriesList = _labels.value?.toMutableList() ?: mutableListOf()
            categoriesList.add(labelWithId)
            _labels.value = categoriesList
        } catch (exception: Exception) {
            Log.d("OFF", "Encountered error while adding category ${exception.message}")
        }
    }

    suspend fun updateLabel(label: Label) {
        val success = labelsRepository.updateLabel(label)
        Log.d("UPDATE", success.toString())
        Log.d("UPDATE", label.icon.icon)
        if (success) {
            val labels = _labels.value as MutableList
            _labels.value = labels.map { l ->
                if (l.labelId != label.labelId)
                    l
                else
                    label
            }
            Log.d("UPDATE", "labels updated")
        }
    }
}