package com.adriancrisan.expensetracker.ui.screens

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.adriancrisan.expensetracker.ui.components.*
import com.adriancrisan.expensetracker.ui.navigation.Routes
import com.adriancrisan.expensetracker.ui.viewmodels.ExpenseModel
import java.time.YearMonth

@Composable
fun MonthlyView(expenseModel: ExpenseModel, navController: NavController) {
    val scrollState = rememberScrollState()
    var selectedMonth: YearMonth by remember { mutableStateOf(YearMonth.now()) }

    LaunchedEffect(selectedMonth) {
        expenseModel.setMonth(selectedMonth)
    }

    val expensesLiveData by expenseModel.monthlyExpenses.observeAsState(listOf())
    val expenses = expensesLiveData

    ExpenseTrackerScaffold(navController = navController, modifier = Modifier.padding(8.dp)) {
        MonthlyExpenseChart()

        MonthPicker(selectedMonth, onChange = { selectedMonth = it })

        TopLabels(expenses)

        Column(
            modifier = Modifier.verticalScroll(scrollState),
            verticalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            expenses.forEach { expense ->
                ExpenseCard(expense = expense, edit = {
                    navController.navigate("${Routes.EditExpense.name}/$it")
                })
            }
        }
    }
}