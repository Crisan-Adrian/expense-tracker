package com.adriancrisan.expensetracker.di

import com.adriancrisan.expensetracker.data.LabelsRepository
import com.adriancrisan.expensetracker.data.ILabelsRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class CategoryRepositoryModule {
    @Binds
    @Singleton
    abstract fun bindCategoryRepo(categoryRepository: LabelsRepository): ILabelsRepository
}