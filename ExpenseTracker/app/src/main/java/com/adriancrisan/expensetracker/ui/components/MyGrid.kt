package com.adriancrisan.expensetracker.ui.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.layout.Placeable
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.*
import kotlin.math.roundToInt

interface MyGridRows {
    fun calculateRows(size: Int, spacing: Int): Int

    class FixedRows(
        private val rows: Int = 3,
    ) : MyGridRows {

        override fun calculateRows(size: Int, spacing: Int): Int {
            return rows
        }

    }

    class AdaptableRows(private val maxHeight: Dp) : MyGridRows {
        init {
            require(maxHeight > 0.dp)
        }

        override fun calculateRows(size: Int, spacing: Int): Int {
            return 1
        }

    }
}

@Composable
fun MyVerticalGrid(
    modifier: Modifier = Modifier,
    arrangement: Arrangement.HorizontalOrVertical = Arrangement.spacedBy(8.dp),
    alignment: Alignment.Horizontal = Alignment.Start,
    content: @Composable () -> Unit,
) {
    Layout(
        modifier = modifier,
        content = content)
    { measurables, constraints ->
//        Log.d("MyGrid", constraints.toString())
        // Create new constraint for measuring children
        val measureConstraint =
            Constraints(maxWidth = constraints.maxWidth, maxHeight = constraints.maxHeight)


        val placeables = measurables.map { measurable ->
            // Measure each children
            measurable.measure(measureConstraint)
        }

        // Calculate row height
        val maxPlaceableHeight =
            if (placeables.isNotEmpty())
                placeables.maxOf { placeable -> placeable.height }
            else
                0

//        Log.d("MyGrid_Placeables",
//            "maxPlaceableHeight: $maxPlaceableHeight, minPlaceableWidth: $minPlaceableWidth")

        // Set the size of the layout as big as it can
        layout(constraints.maxWidth, constraints.maxHeight) {

            var placedIndex = 0
            val toPlace = mutableListOf<Placeable>()

            var yPosition = 0
            var xPosition = 0

            // Place all placeables
            while (placedIndex < placeables.size) {
                var accumulatedWidth = 0
                var widthCondition: Int
                // Get as many placeables as you can fit in one line
                do {
                    toPlace.add(placeables[placedIndex])

                    // Add placeable width + spacing
                    accumulatedWidth += placeables[placedIndex].width + arrangement.spacing.toPx()
                        .roundToInt()
                    placedIndex++

                    if (placedIndex == placeables.size) {
                        break
                    } else {
                        widthCondition = accumulatedWidth + placeables[placedIndex].width
                    }

                } while (widthCondition < constraints.maxWidth)

                // Remove last spacing added
                accumulatedWidth -= arrangement.spacing.toPx().roundToInt()
//                if (accumulatedWidth > constraints.maxWidth) {
//                    Log.d("MyGrid_Accumulated",
//                        "accumulatedWidth: $accumulatedWidth, maxWidth: ${constraints.maxWidth}")
//                }

                // Calculate padding to center row
                val sidePadding = (constraints.maxWidth - accumulatedWidth)

                xPosition += when (alignment) {
                    Alignment.CenterHorizontally -> sidePadding / 2
                    Alignment.End -> sidePadding
                    Alignment.Start -> 0
                    else -> 0
                }

                // Place one row
                toPlace.forEach { placeable ->
                    placeable.placeRelative(xPosition, yPosition)
                    xPosition += arrangement.spacing.toPx().roundToInt() + placeable.width
                }

                // Shift line down
                yPosition += arrangement.spacing.toPx().roundToInt() + maxPlaceableHeight

                // Reset variables
                xPosition = 0
                toPlace.clear()
            }
        }
    }
}

@Composable
fun MyHorizontalGrid(
    gridRows: MyGridRows,
    modifier: Modifier = Modifier,
    contentPadding: PaddingValues = PaddingValues(0.dp),
    arrangement: Arrangement.HorizontalOrVertical = Arrangement.spacedBy(8.dp),
    content: @Composable () -> Unit,
) {
    Layout(
        modifier = modifier,
        content = content)
    { measurables, constraints ->
//        Log.d("MyGrid", constraints.toString())
        // Create new constraint for measuring children
        val measureConstraint =
            Constraints(maxWidth = constraints.maxWidth, maxHeight = constraints.maxHeight)


        val placeables = measurables.map { measurable ->
            // Measure each children
            measurable.measure(measureConstraint)
        }

        val topPadding = contentPadding.calculateTopPadding().toPx().roundToInt()
        val bottomPadding = contentPadding.calculateBottomPadding().toPx().roundToInt()
        val leftPadding =
            contentPadding.calculateLeftPadding(LayoutDirection.Ltr).toPx().roundToInt()
        val rightPadding =
            contentPadding.calculateRightPadding(LayoutDirection.Ltr).toPx().roundToInt()
        val spacing = arrangement.spacing.toPx().roundToInt()

        // Calculate row height
        val maxPlaceableWidth =
            if (placeables.isNotEmpty())
                placeables.maxOf { placeable -> placeable.width }
            else
                0

        val maxPlaceableHeight =
            if (placeables.isNotEmpty())
                placeables.maxOf { placeable -> placeable.height }
            else
                0

////        Log.d("MyGrid_Placeables",
//            "maxPlaceableHeight: $maxPlaceableHeight, minPlaceableWidth: $minPlaceableWidth")

        var placedIndex = 0
        val toPlace = mutableListOf<Placeable>()

        val rows =
            gridRows.calculateRows(maxPlaceableWidth, spacing)
        var column = 0
        var yPositionTEMP = 0
        val xPositions = MutableList(rows) { leftPadding }
        val positions: MutableMap<Placeable, Pair<Int, Int>> = mutableMapOf()
        val widths = MutableList(rows) { leftPadding + rightPadding }

        // ---------------------------------------
        // Calculate all placeables' positions
        while (placedIndex < placeables.size) {
            var accumulatedHeight = 0
            var row = 0
            var heightCondition: Int
            // Get as many placeables as you can fit in one column
            do {
                toPlace.add(placeables[placedIndex])

                // Add placeable height + spacing
                accumulatedHeight += maxPlaceableHeight + arrangement.spacing.toPx()
                    .roundToInt()
                placedIndex++

                if (placedIndex == placeables.size) {
                    break
                } else {
                    row++
                    heightCondition = accumulatedHeight + placeables[placedIndex].height
                }
            } while (row < rows && heightCondition < constraints.maxHeight)

            // Remove last spacing added
            accumulatedHeight -= spacing
//                if (accumulatedWidth > constraints.maxWidth) {
//                    Log.d("MyGrid_Accumulated",
//                        "accumulatedWidth: $accumulatedWidth, maxWidth: ${constraints.maxWidth}")
//                }

            // Calculate padding to center row
            val sidePadding = (constraints.maxHeight - accumulatedHeight)
            yPositionTEMP += topPadding


            // Place one column
            var index = 0
            toPlace.forEach { placeable ->
                var xPositionTEMP = xPositions[index]

                val placeableSidePadding = (maxPlaceableHeight - placeable.height) / 2
                yPositionTEMP += placeableSidePadding
                positions[placeable] = Pair(xPositionTEMP, yPositionTEMP)

                yPositionTEMP += spacing +
                        placeable.height + placeableSidePadding

                xPositionTEMP += placeable.width + spacing
                xPositions[index] = xPositionTEMP
                widths[index] += placeable.width + spacing
                index++
            }

            // Reset variables
            yPositionTEMP = 0
            column++
            toPlace.clear()
        }
        // ---------------------------------------

        val maxHeight =
            maxPlaceableHeight * rows + spacing * (rows - 1)
        val maxWidth =
            widths.maxOf { width -> width }

        // Set the size of the layout as big as it can
        layout(maxWidth, maxHeight) {
            for (placeable in positions.keys) {
                val xPosition: Int = positions[placeable]?.first ?: 0
                val yPosition: Int = positions[placeable]?.second ?: 0

                placeable.placeRelative(xPosition, yPosition)
            }
        }
    }
}