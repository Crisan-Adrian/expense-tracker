package com.adriancrisan.expensetracker.ui.components

enum class TextFieldType {
    Filled,
    Outlined,
}