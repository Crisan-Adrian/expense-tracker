package com.adriancrisan.expensetracker.data

import android.util.Log
import com.adriancrisan.expensetracker.data.dao.LabelDAO
import com.adriancrisan.expensetracker.data.entities.Label
import javax.inject.Inject

class LabelsRepository @Inject constructor(
    private val db: AppDatabase
): ILabelsRepository {
    private val labelDAO: LabelDAO = db.labelDao()

    override suspend fun getAll(): List<Label> {
        return labelDAO.getAll()
    }

    override suspend fun get(labelId: Long): Label {
        return labelDAO.get(labelId)
    }

    override suspend fun addLabel(label: Label): Long {
        return labelDAO.insertAll(label)[0]
    }

    override suspend fun updateLabel(label: Label): Boolean {
        val returned = labelDAO.updateLabel(label)
        Log.d("UPDATE", "Repo value: $returned")
        return returned != 0
    }
}