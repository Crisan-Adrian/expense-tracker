package com.adriancrisan.expensetracker.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.adriancrisan.expensetracker.data.dao.LabelDAO
import com.adriancrisan.expensetracker.data.dao.ExpenseDAO
import com.adriancrisan.expensetracker.data.dao.ExpenseLabelsCrossRefDAO
import com.adriancrisan.expensetracker.data.entities.Expense
import com.adriancrisan.expensetracker.data.entities.ExpenseLabelCrossRef
import com.adriancrisan.expensetracker.data.entities.Label
import com.adriancrisan.expensetracker.utils.DateTimeConverter

@Database(entities = [Expense::class, Label::class, ExpenseLabelCrossRef::class],
    version = 1,
    exportSchema = true,
    autoMigrations = [
    ]
)
@TypeConverters(DateTimeConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun expenseDao() : ExpenseDAO
    abstract fun labelDao() : LabelDAO
    abstract fun expenseLabelCrossRefDao() : ExpenseLabelsCrossRefDAO
}