package com.adriancrisan.expensetracker.data.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.adriancrisan.expensetracker.data.entities.ExpenseLabelCrossRef
import com.adriancrisan.expensetracker.data.entities.ExpenseWithLabels

data class ExpenseId(
    val expenseId: Long,
)

@Dao
interface ExpenseLabelsCrossRefDAO {
    @Insert
    suspend fun addAll(vararg expenseLabelCrossRef: ExpenseLabelCrossRef)

    @Delete
    suspend fun delete(expenseLabelCrossRef: ExpenseLabelCrossRef)

    @Delete(entity = ExpenseLabelCrossRef::class)
    suspend fun deleteByExpenseId(vararg idCategory: ExpenseId)
}