package com.adriancrisan.expensetracker.ui.screens

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.rememberCoroutineScope
import androidx.navigation.NavHostController
import com.adriancrisan.expensetracker.data.entities.ExpenseWithLabels
import com.adriancrisan.expensetracker.data.entities.Label
import com.adriancrisan.expensetracker.ui.components.ExpenseForm
import com.adriancrisan.expensetracker.ui.viewmodels.LabelsModel
import kotlinx.coroutines.launch
import java.time.OffsetDateTime

@Composable
fun NewExpenseScreen(
    navController: NavHostController,
    labelsModel: LabelsModel,
    addExpense: suspend (ExpenseWithLabels) -> Unit,
) {
    val labels: List<Label> by labelsModel.labels.observeAsState(listOf())
    val coroutineScope = rememberCoroutineScope()

    val add: suspend (ExpenseWithLabels) -> Unit = { expense ->
        addExpense(
            expense
        )
    }

    val getDefaultExpense: () -> ExpenseWithLabels = {
        val tempExpenseWithLabels = ExpenseWithLabels.Default
        val tempExpense = tempExpenseWithLabels.expense.copy(date= OffsetDateTime.now())
        tempExpenseWithLabels.copy(expense = tempExpense)
    }

    ExpenseForm(expense = getDefaultExpense(),
        labels = labels,
        onSubmit = { expense ->
            coroutineScope.launch {
                add(expense)
            }
            navController.popBackStack()
        },
        onCancel = { navController.popBackStack() },
        title = { Text("Add New Expense") },
        submitButtonText = {
            Text("Add")
        }
    )
}





