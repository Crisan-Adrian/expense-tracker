package com.adriancrisan.expensetracker.ui.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.SmallFloatingActionButton
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.adriancrisan.expensetracker.R
import com.adriancrisan.expensetracker.ui.components.DailySpending
import com.adriancrisan.expensetracker.ui.components.ExpenseCard
import com.adriancrisan.expensetracker.ui.components.ExpenseTrackerScaffold
import com.adriancrisan.expensetracker.ui.components.MonthlyOverview
import com.adriancrisan.expensetracker.ui.navigation.Routes
import com.adriancrisan.expensetracker.ui.viewmodels.ExpenseModel
import kotlinx.coroutines.launch
import java.time.LocalTime
import java.time.OffsetDateTime

@Composable
fun OverviewScreen(viewModel: ExpenseModel, navController: NavController) {
    val date  = OffsetDateTime.now().with(LocalTime.MIN)
    LaunchedEffect(viewModel, date) {
        viewModel.setDay(date)
    }
    val expenses by viewModel.dailyExpenses.observeAsState(listOf())
    val expensesSorted = expenses.sortedBy { expense -> expense.date }.reversed()

    val scrollState = rememberScrollState()

    ExpenseTrackerScaffold(
        navController = navController,
        modifier = Modifier.padding(8.dp),
        fab = {
            Column(
                horizontalAlignment = Alignment.End,
                verticalArrangement = Arrangement.spacedBy(8.dp)
            ) {
                SmallFloatingActionButton(
                    onClick = { navController.navigate(Routes.NewExpenseScan.name) }
                ) {
                    Icon(
                        painter = painterResource(R.drawable.ic_barcode_scanner),
                        contentDescription = "Scan expense"
                    )
                }
                FloatingActionButton(onClick = { navController.navigate(Routes.NewExpense.name) }) {
                    Icon(Icons.Default.Add, contentDescription = "Add expense")
                }
            }
        }
    ) {
        DailySpending(expenses)
        Column(
            verticalArrangement = Arrangement.spacedBy(8.dp),
            modifier = Modifier.verticalScroll(scrollState)
        ) {
            MonthlyOverview()

            expensesSorted.forEach { expense ->
                ExpenseCard(expense = expense, edit = {
                    navController.navigate("${Routes.EditExpense.name}/$it")
                })
            }
        }
    }
}