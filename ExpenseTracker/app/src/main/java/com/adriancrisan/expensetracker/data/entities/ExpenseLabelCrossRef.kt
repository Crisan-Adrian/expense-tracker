package com.adriancrisan.expensetracker.data.entities

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Junction
import androidx.room.Relation
import java.time.OffsetDateTime
import java.time.ZoneOffset

@Entity(primaryKeys = ["expenseId", "labelId"])
data class ExpenseLabelCrossRef(
    val expenseId: Long,
    val labelId: Long,
)

data class ExpenseWithLabels(
    @Embedded val expense: Expense,
    @Relation(
        parentColumn = "expenseId",
        entityColumn = "labelId",
        associateBy = Junction(ExpenseLabelCrossRef::class)
    )
    val labels: List<Label>,
) {
    companion object {
        private val defaultExpense = Expense.Default
        private val defaultLabels: List<Label> = listOf()
        val Default: ExpenseWithLabels = ExpenseWithLabels(
            defaultExpense,
            defaultLabels
        )
    }
}


data class LabelWithExpenses(
    @Embedded val label: Label,
    @Relation(
        parentColumn = "labelId",
        entityColumn = "expenseId",
        associateBy = Junction(ExpenseLabelCrossRef::class)
    )
    val expenses: List<Expense>,
)