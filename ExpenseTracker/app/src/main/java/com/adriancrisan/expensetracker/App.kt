package com.adriancrisan.expensetracker

import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.adriancrisan.expensetracker.ui.screens.MonthlyView
import com.adriancrisan.expensetracker.ui.navigation.Routes
import com.adriancrisan.expensetracker.ui.screens.*
import com.adriancrisan.expensetracker.ui.viewmodels.LabelsModel
import com.adriancrisan.expensetracker.ui.viewmodels.ExpenseModel

@Composable
fun App(
    expenseModel: ExpenseModel = hiltViewModel(),
    labelsModel: LabelsModel = hiltViewModel(),
    navController: NavHostController = rememberNavController(),
    startDestination: String = Routes.Overview.name
) {
    NavHost(navController = navController, startDestination = startDestination) {
        composable(Routes.Overview.name) { OverviewScreen(expenseModel, navController) }
        composable(Routes.Monthly.name) { MonthlyView(expenseModel, navController) }
        composable(Routes.Budget.name) { BudgetScreen(navController, labelsModel) }
        composable(Routes.Settings.name) { SettingsScreen(navController) }
        composable(Routes.Profile.name) { ProfileScreen(navController) }

        composable(Routes.NewExpense.name) { NewExpenseScreen(navController, labelsModel, expenseModel::addExpense) }
        composable("${Routes.EditExpense.name}/{labelId}",
            arguments = listOf(navArgument("labelId") {type = NavType.LongType})
        ) { backStackEntry ->
            EditExpenseScreen(navController, expenseModel, labelsModel,backStackEntry.arguments?.getLong("labelId") ?: -1)
        }

        composable(Routes.NewLabel.name) { NewLabelScreen(navController, labelsModel, labelsModel::addLabel) }
        composable(
            "${Routes.EditLabel.name}/{labelId}",
            arguments = listOf(navArgument("labelId") {type = NavType.LongType})
        ) { backStackEntry ->
            EditLabelScreen(navController, labelsModel, backStackEntry.arguments?.getLong("labelId") ?: -1) }
        /*...*/
    }
}