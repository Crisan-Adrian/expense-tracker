package com.adriancrisan.expensetracker.ui.components

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.SuggestionChip
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.adriancrisan.expensetracker.data.entities.Label

@Composable
@OptIn(ExperimentalMaterial3Api::class)
fun LabelSuggestionChip(
    label: Label,
    onClick: (Label) -> Unit,
    selected: Boolean,
    modifier: Modifier = Modifier,
) {
    SuggestionChip(
        onClick = {
            onClick(label)
        },
        modifier = modifier,
        label = {
            Text(label.name)
        },
        icon = {
            if (selected) {
                Icon(Icons.Filled.Check,
                    contentDescription = null)
            } else {
                LabelIcon(label.icon)
            }
        }
    )
}
