package com.adriancrisan.expensetracker.ui.screens

import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.navigation.NavHostController
import com.adriancrisan.expensetracker.data.entities.ExpenseWithLabels
import com.adriancrisan.expensetracker.data.entities.Label
import com.adriancrisan.expensetracker.ui.components.ExpenseForm
import com.adriancrisan.expensetracker.ui.viewmodels.ExpenseModel
import com.adriancrisan.expensetracker.ui.viewmodels.LabelsModel
import kotlinx.coroutines.launch

@Composable
fun EditExpenseScreen(
    navController: NavHostController,
    expenseModel: ExpenseModel,
    labelsModel: LabelsModel,
    expenseId: Long,
) {
    val labels: List<Label> by labelsModel.labels.observeAsState(listOf())
    var expense: ExpenseWithLabels by remember { mutableStateOf(ExpenseWithLabels.Default) }
    var loaded by remember { mutableStateOf(false) }

    val coroutineScope = rememberCoroutineScope()

    LaunchedEffect(key1 = coroutineScope) {
        expense = expenseModel.getExpenseWithLabels(expenseId)
        loaded = true
    }

    val add: suspend (ExpenseWithLabels) -> Unit = { updatedExpense ->
        expenseModel.updateExpenseWithLabels(updatedExpense)
    }

    if (!loaded) {
        CircularProgressIndicator()
    } else {
        ExpenseForm(
            expense = expense,
            labels = labels,
            onSubmit = { submittedExpense ->
                coroutineScope.launch {
                    add(submittedExpense)
                }
                navController.popBackStack()
            },
            onCancel = {
                navController.popBackStack()
            },
            title = { Text("Add New Expense") },
            submitButtonText = { Text("Update") },
        )
    }
}
