package com.adriancrisan.expensetracker.data

import com.adriancrisan.expensetracker.data.entities.Label

interface ILabelsRepository {
    suspend fun getAll() : List<Label>

    suspend fun get(labelId: Long): Label

    suspend fun addLabel(label: Label) : Long

    suspend fun updateLabel(label: Label) : Boolean
}