package com.adriancrisan.expensetracker.di

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.adriancrisan.expensetracker.data.AppDatabase
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@dagger.Module
@InstallIn(SingletonComponent::class)
class ProvideModule {

    @Provides
    @Singleton
    fun providesDatabase(application: Application): AppDatabase {
        return Room.databaseBuilder(
            application.applicationContext,
            AppDatabase::class.java, "expense_tracker_db"
        ).build()
    }
}