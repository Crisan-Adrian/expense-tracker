package com.adriancrisan.expensetracker.data

import com.adriancrisan.expensetracker.data.dao.ExpenseDAO
import com.adriancrisan.expensetracker.data.dao.ExpenseId
import com.adriancrisan.expensetracker.data.dao.ExpenseLabelsCrossRefDAO
import com.adriancrisan.expensetracker.data.entities.Expense
import com.adriancrisan.expensetracker.data.entities.ExpenseLabelCrossRef
import com.adriancrisan.expensetracker.data.entities.ExpenseWithLabels
import javax.inject.Inject

class ExpenseRepository @Inject constructor(
    private val db: AppDatabase,
) : IExpenseRepository {
    private var expenseDAO: ExpenseDAO = db.expenseDao()
    private var expenseLabelsCrossRefDAO: ExpenseLabelsCrossRefDAO = db.expenseLabelCrossRefDao()

    override suspend fun getExpenses(): List<Expense> {
        return expenseDAO.getAll()
    }

    override suspend fun getExpenseWithLabels(expenseId: Long): ExpenseWithLabels {
        return expenseDAO.getWithLabels(expenseId)
    }

    override suspend fun updateExpenseWithLabels(expense: ExpenseWithLabels): Boolean {
        val rows = expenseDAO.updateExpenses(expense.expense)
        expenseLabelsCrossRefDAO.deleteByExpenseId(ExpenseId(expenseId = expense.expense.expenseId!!))
        val expenseLabelsCrossRef = expense.labels.map { label ->
            ExpenseLabelCrossRef(
                expense.expense.expenseId,
                label.labelId ?: -1
            )
        }.toTypedArray()

        expenseLabelsCrossRefDAO.addAll(*expenseLabelsCrossRef)
        return rows != 0
    }

    override suspend fun addExpense(expense: ExpenseWithLabels): Long {
        val expenseId = expenseDAO.insertAll(expense.expense)[0]
        val expenseLabelsCrossRef = expense.labels.map { label ->
            ExpenseLabelCrossRef(
                expenseId,
                label.labelId ?: -1
            )
        }.toTypedArray()

        expenseLabelsCrossRefDAO.addAll(*expenseLabelsCrossRef)
        return expenseId
    }
}