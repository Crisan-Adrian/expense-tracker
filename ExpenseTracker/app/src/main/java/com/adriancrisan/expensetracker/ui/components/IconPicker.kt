package com.adriancrisan.expensetracker.ui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.adriancrisan.expensetracker.data.entities.LabelIcon
import com.adriancrisan.expensetracker.ui.icons.MyIcons

@Composable
fun IconPicker(
    icons: List<LabelIcon>,
    selected: LabelIcon?,
    onSelectionChange: (LabelIcon) -> Unit,
    modifier: Modifier = Modifier,
) {
    val color = MaterialTheme.colorScheme.surface
    val selectedColor = MaterialTheme.colorScheme.surfaceVariant

    LazyVerticalGrid(
        columns = GridCells.Adaptive(54.dp),
        horizontalArrangement = Arrangement.spacedBy(8.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp),
        modifier = modifier,
    ) {
        items(icons, key = { icon ->
            icon.icon
        }) { icon ->
            Card(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(8.dp)
                    .aspectRatio(1f),
                elevation = CardDefaults.outlinedCardElevation(),
                colors = CardDefaults.cardColors(
                    containerColor = if (icon == selected) selectedColor else color
                )
            ) {
                Box(
                    modifier = Modifier
                        .clickable(
                            enabled = true,
                            onClick = { onSelectionChange(icon) },
                        )
                        .fillMaxSize(),
                    contentAlignment = Alignment.Center,
                ) {
                    LabelIcon(labelIcon = icon, modifier = Modifier
                        .padding(8.dp)
                        .size(30.dp))
                }

            }
        }
    }
}

@Preview
@Composable
fun IconPickerPreview() {
    val icons = MyIcons.getIconList()
    var selected: LabelIcon? by remember { mutableStateOf(null) }

    IconPicker(
        icons = icons,
        selected = selected,
        onSelectionChange = { icon ->
            selected = icon
        })
}