package com.adriancrisan.expensetracker.ui.components

import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.*
import java.lang.Integer.max
import java.text.DecimalFormat

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CurrencyField(
    value: String,
    onValueChange: (String) -> Unit,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    readOnly: Boolean = false,
    textStyle: TextStyle = LocalTextStyle.current,
    label: @Composable (() -> Unit)? = null,
    placeholder: @Composable (() -> Unit)? = null,
    leadingIcon: @Composable (() -> Unit)? = null,
    trailingIcon: @Composable (() -> Unit)? = null,
    supportingText: @Composable (() -> Unit)? = null,
    isError: Boolean = false,
    keyboardActions: KeyboardActions = KeyboardActions.Default,
    singleLine: Boolean = false,
    maxLines: Int = if (singleLine) 1 else Int.MAX_VALUE,
    minLines: Int = 1,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    type: TextFieldType = TextFieldType.Outlined,
    shape: Shape = if (type == TextFieldType.Outlined) TextFieldDefaults.outlinedShape else TextFieldDefaults.filledShape,
    colors: TextFieldColors = if (type == TextFieldType.Outlined) TextFieldDefaults.outlinedTextFieldColors() else TextFieldDefaults.textFieldColors(),
    fixCursorAtTheEnd: Boolean = true,
    decimalPlaces: Int = 2,
    groupingSeparator: Char = ' ',
    decimalSeparator: Char = '.',
) {
    val currencyVisualTransformation = CurrencyVisualTransformation(
        fixCursorAtTheEnd = fixCursorAtTheEnd,
        decimalPlaces = decimalPlaces,
        groupingSeparator = groupingSeparator,
        decimalSeparator = decimalSeparator,
    )

    when (type) {
        TextFieldType.Filled -> TextField(
            value = value,
            onValueChange = onValueChange,
            modifier = modifier,
            enabled = enabled,
            readOnly = readOnly,
            textStyle = textStyle,
            label = label,
            placeholder = placeholder,
            leadingIcon = leadingIcon,
            trailingIcon = trailingIcon,
            supportingText = supportingText,
            isError = isError,
            keyboardActions = keyboardActions,
            singleLine = singleLine,
            maxLines = maxLines,
            minLines = minLines,
            interactionSource = interactionSource,
            shape = shape,
            colors = colors,
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.NumberPassword,
                imeAction = ImeAction.Done
            ),
            visualTransformation = currencyVisualTransformation,
        )
        TextFieldType.Outlined -> OutlinedTextField(
            value = value,
            onValueChange = onValueChange,
            modifier = modifier,
            enabled = enabled,
            readOnly = readOnly,
            textStyle = textStyle,
            label = label,
            placeholder = placeholder,
            leadingIcon = leadingIcon,
            trailingIcon = trailingIcon,
            supportingText = supportingText,
            isError = isError,
            keyboardActions = keyboardActions,
            singleLine = singleLine,
            maxLines = maxLines,
            minLines = minLines,
            interactionSource = interactionSource,
            shape = shape,
            colors = colors,
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.NumberPassword,
                imeAction = ImeAction.Done
            ),
            visualTransformation = currencyVisualTransformation,
        )
    }

}

class CurrencyVisualTransformation(
    private val fixCursorAtTheEnd: Boolean = true,
    private val decimalPlaces: Int = 2,
    groupingSeparator: Char = ' ',
    decimalSeparator: Char = '.',
) : VisualTransformation {
    private val symbols = DecimalFormat().decimalFormatSymbols

    init {
        symbols.groupingSeparator = groupingSeparator
        symbols.decimalSeparator = decimalSeparator
    }

    override fun filter(text: AnnotatedString): TransformedText {
        val thousandsSeparator = symbols.groupingSeparator
        val decimalSeparator = symbols.decimalSeparator
        val zero = symbols.zeroDigit

        val inputText = text.text

        val intPart = inputText
            .dropLast(decimalPlaces)
            .reversed()
            .chunked(3)
            .joinToString(thousandsSeparator.toString())
            .reversed()
            .ifEmpty { zero.toString() }

        val fractionPart = inputText.takeLast(decimalPlaces).let {
            if (it.length != decimalPlaces) {
                List(decimalPlaces - it.length) {
                    zero
                }.joinToString("") + it
            } else {
                it
            }
        }

        val formatterNumber = intPart + decimalSeparator.toString() + fractionPart

        val newText = AnnotatedString(
            text = formatterNumber,
            spanStyles = text.spanStyles,
            paragraphStyles = text.paragraphStyles
        )
        val offsetMapping = if (fixCursorAtTheEnd) {
            FixedCursorMapping(
                contentLength = inputText.length,
                formattedContentLength = formatterNumber.length
            )
        } else {
            MovableCursorMapping(
                unmaskedText = text.toString(),
                maskedText = newText.toString(),
                decimalPlaces = decimalPlaces
            )
        }

        return TransformedText(newText, offsetMapping)
    }

}

private class FixedCursorMapping(
    private val contentLength: Int,
    private val formattedContentLength: Int,
) : OffsetMapping {
    override fun originalToTransformed(offset: Int): Int = formattedContentLength

    override fun transformedToOriginal(offset: Int): Int = contentLength
}

private class MovableCursorMapping(
    private val unmaskedText: String,
    private val maskedText: String,
    private val decimalPlaces: Int
) : OffsetMapping {
    override fun originalToTransformed(offset: Int): Int =
        when {
            unmaskedText.length <= decimalPlaces -> {
                maskedText.length - (unmaskedText.length - offset)
            }
            else -> {
                offset + offsetMaskCount(offset, maskedText)
            }
        }

    override fun transformedToOriginal(offset: Int): Int =
        when {
            unmaskedText.length <= decimalPlaces -> {
                max(unmaskedText.length - (maskedText.length - offset), 0)
            }
            else -> {
                offset - maskedText.take(offset).count { !it.isDigit() }
            }
        }

    private fun offsetMaskCount(offset: Int, maskedText: String): Int {
        var maskOffsetCount = 0
        var dataCount = 0
        for (maskedChar in maskedText) {
            if (!maskedChar.isDigit()) {
                maskOffsetCount++
            } else if (++dataCount > offset) {
                break
            }
        }
        return maskOffsetCount
    }

}