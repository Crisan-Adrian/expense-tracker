package com.adriancrisan.expensetracker.di

import com.adriancrisan.expensetracker.data.ExpenseRepository
import com.adriancrisan.expensetracker.data.IExpenseRepository
import dagger.Binds
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@dagger.Module
@InstallIn(SingletonComponent::class)
abstract class BindModule {

    @Binds
    @Singleton
    abstract fun bindExpenseRepo(expenseRepository: ExpenseRepository): IExpenseRepository
}