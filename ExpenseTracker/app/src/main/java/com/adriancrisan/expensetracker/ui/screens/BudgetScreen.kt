package com.adriancrisan.expensetracker.ui.screens

import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.adriancrisan.expensetracker.data.entities.Label
import com.adriancrisan.expensetracker.ui.components.ExpenseTrackerScaffold
import com.adriancrisan.expensetracker.ui.components.LabelsCard
import com.adriancrisan.expensetracker.ui.viewmodels.LabelsModel

@Composable
fun BudgetScreen(navController: NavController, labelsModel: LabelsModel) {
    ExpenseTrackerScaffold(
        navController = navController,
        modifier = Modifier.padding(0.dp, 8.dp)
    ) {
        val labels: List<Label> by labelsModel.labels.observeAsState(listOf())

        LabelsCard(
            navController = navController,
            labels = labels,
        )
    }
}