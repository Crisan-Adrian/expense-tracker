package com.adriancrisan.expensetracker.ui.components

import android.util.Log
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.adriancrisan.expensetracker.ui.navigation.Routes


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ExpenseTrackerScaffold(
    navController: NavController,
    modifier: Modifier = Modifier,
    fab: @Composable () -> Unit = {},
    content: @Composable () -> Unit,
) {
    val navBarRoutes = listOf(Routes.Overview, Routes.Monthly, Routes.Budget)
    val navBarIcons =
        listOf(Icons.Default.Home, Icons.Default.CalendarMonth, Icons.Default.AccountBalanceWallet)
    var selectedItem by remember { mutableStateOf(0) }
    val defaultRoute = navBarRoutes[0]
    navController.addOnDestinationChangedListener { _, destination, _ ->
        Log.d("", destination.route.toString())
        val destinationRoute = destination.route ?: ""
        for(route in navBarRoutes) {
            if(destinationRoute.matches(Regex.fromLiteral(route.name))) {
                selectedItem =
                    navBarRoutes.indexOf(route)
            } else {
                selectedItem = navBarRoutes.indexOf(defaultRoute)
            }
        }
    }

    Scaffold(
        topBar = {
            CenterAlignedTopAppBar(title = { Text("ExpenseTrackr") },
                actions = {
                    IconButton(onClick = { navController.navigate(Routes.Settings.name) }) {
                        Icon(Icons.Default.Settings, contentDescription = "Settings")
                    }
                    IconButton(onClick = { navController.navigate(Routes.Profile.name) }) {
                        Icon(Icons.Default.Person, contentDescription = "Profile")
                    }
                })
        },
        bottomBar = {
            NavigationBar()
            {
                navBarRoutes.forEachIndexed { index, item ->
                    NavigationBarItem(
                        label = { Text(item.toString()) },
                        icon = {
                            Icon(
                                navBarIcons[index],
                                contentDescription = "Icon"
                            )
                        },
                        selected = selectedItem == index,
                        onClick = {
                            selectedItem = index
                            navController.navigate(item.toString())
                        })
                }
            }
        },
        floatingActionButton = { fab() },
        content = { innerPadding ->
            Column(modifier = modifier.padding(innerPadding),
                verticalArrangement = Arrangement.spacedBy(8.dp)) {
                content()
            }
        }
    )
}