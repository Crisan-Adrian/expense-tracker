package com.adriancrisan.expensetracker.ui.components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MonthlyOverview() {
    Card(onClick = {}) {
    Box(
        Modifier
            .fillMaxWidth()
            .padding(all = 8.dp)) {
        Text("Monthly Spending")
    }
}
}