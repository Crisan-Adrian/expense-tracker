package com.adriancrisan.expensetracker.ui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.adriancrisan.expensetracker.data.entities.Expense

@Composable
fun ExpenseCard(
    expense: Expense,
    modifier: Modifier = Modifier,
    edit: (Long) -> Unit,
) {
    Card(
        elevation = CardDefaults.elevatedCardElevation(),
        modifier = modifier
            .fillMaxWidth()
            .clickable {
                edit(expense.expenseId!!)
            }
    ) {
        Column(modifier = Modifier.padding(8.dp)) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.Top
            ) {
                Text(
                    text = expense.name,
                    fontSize = 24.sp
                )
                Text(
                    text = "${expense.date.toLocalDate()}"
                )
            }
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.Bottom
            ) {
                Spacer(Modifier.weight(1f))
                Text(
                    text = "${expense.value} RON",
                    fontSize = 24.sp
                )
            }

        }
    }
}