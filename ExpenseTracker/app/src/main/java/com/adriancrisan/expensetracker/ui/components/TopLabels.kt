package com.adriancrisan.expensetracker.ui.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.unit.dp
import com.adriancrisan.expensetracker.data.entities.Expense
import com.adriancrisan.expensetracker.data.entities.Label

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TopLabels(expenseList: List<Expense>) {
//    val groupedExpenses = expenseList.groupBy { expense -> expense.labels }
//    val categorySpending = mutableMapOf<Label?, Float>()
//    for (entry in groupedExpenses) {
//        var spending = 0f
//        entry.value.forEach { expense -> spending += expense.value }
//        categorySpending[entry.key] = spending
//    }
//
//    val topCategories = categorySpending.toList().sortedBy { (_, value) -> value }.reversed().toMap()
//
//    Row(horizontalArrangement = Arrangement.spacedBy(8.dp)) {
//        for (category in topCategories) {
//            FilterChip(
//                selected = false,
//                onClick = { },
//                label = { Text(text = category.key?.name ?: "Uncategorized") })
//        }
//    }
}