package com.adriancrisan.expensetracker.data

import com.adriancrisan.expensetracker.data.entities.Expense
import com.adriancrisan.expensetracker.data.entities.ExpenseWithLabels

interface IExpenseRepository {
    suspend fun getExpenses() : List<Expense>

    suspend fun getExpenseWithLabels(expenseId: Long) : ExpenseWithLabels

    suspend fun updateExpenseWithLabels(expense: ExpenseWithLabels) : Boolean

    suspend fun addExpense(expense: ExpenseWithLabels) : Long
}