package com.adriancrisan.expensetracker.data.entities

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Abc
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.core.graphics.drawable.IconCompat.IconType
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "labels")
data class Label(
    @PrimaryKey(autoGenerate = true)
    val labelId: Long? = null,
    val name: String,
    @Embedded(prefix = "icon_")
    val icon: LabelIcon = LabelIcon.Default,
)

class LabelIcon(
    val icon: String,
    val type: LabelIconType,
) {
    companion object {
        private val defaultIcon = Icons.Default.Abc.name.split(".").last()
        val Default =
            LabelIcon(icon = defaultIcon, type = LabelIconType.VECTOR)
    }

    override fun equals(other: Any?): Boolean {
        if (other !is LabelIcon) {
            return false
        }
        return icon == other.icon && type == other.type
    }

    override fun hashCode(): Int {
        var result = icon.hashCode()
        result = 31 * result + type.hashCode()
        return result
    }
}

enum class LabelIconType {
    VECTOR,
    PAINTER,
}