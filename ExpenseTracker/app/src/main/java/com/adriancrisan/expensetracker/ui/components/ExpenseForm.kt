package com.adriancrisan.expensetracker.ui.components

import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.unit.dp
import com.adriancrisan.expensetracker.data.entities.ExpenseWithLabels
import com.adriancrisan.expensetracker.data.entities.Label
import com.adriancrisan.expensetracker.utils.format


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ExpenseForm(
    expense: ExpenseWithLabels,
    labels: List<Label>,
    onSubmit: (ExpenseWithLabels) -> Unit,
    onCancel: () -> Unit,
    title: @Composable () -> Unit,
    submitButtonText: @Composable () -> Unit,
) {
    val scrollState = rememberScrollState()
    val focusManager = LocalFocusManager.current

    var name by remember { mutableStateOf(expense.expense.name) }
    val initialValue = expense.expense.value.format(2).split(".").joinToString("").trim('0')
    var value by remember { mutableStateOf(initialValue) }
    val selectedLabels: MutableList<Label> = remember(expense) { mutableStateListOf() }

    var date by remember { mutableStateOf(expense.expense.date) }
    var tempDay by remember { mutableStateOf(date.dayOfMonth) }
    var tempMonth by remember { mutableStateOf(date.monthValue) }
    var tempYear by remember { mutableStateOf(date.year) }

    LaunchedEffect(key1 = expense) {
        expense.labels.forEach { l ->
            if (!selectedLabels.contains(l)) {
                selectedLabels.add(l)
            }
        }
    }

    val submit: () -> Unit = {
        val valueWithDecimalPoint =
            (if (value.length > 2)
                value.take(value.length - 2) + '.' + value.takeLast(2)
            else if (value.length == 2)
                "0.$value"
            else
                "0.0$value").toFloat()
        date = date
            .withDayOfMonth(tempDay)
            .withMonth(tempMonth)
            .withYear(tempYear)
        val updatedExpense = expense.expense.copy(
            expenseId = expense.expense.expenseId,
            name = name,
            value = valueWithDecimalPoint,
            date = date
        )
        val updatedExpenseWithLabels = ExpenseWithLabels(updatedExpense, selectedLabels)
        onSubmit(updatedExpenseWithLabels)
    }

    Scaffold(
        topBar = {
            CenterAlignedTopAppBar(
                title = { title() },
                navigationIcon = {
                    IconButton(onClick = { /*navController.popBackStack()*/ onCancel() }) {
                        Icon(Icons.Filled.Close, contentDescription = null)
                    }
                },
                actions = {
                    TextButton(
                        onClick = {
                            submit()
                        }
                    ) {
                        submitButtonText()
                    }
                }
            )
        }
    ) { paddingValues ->
        Column(
            modifier = Modifier
                .padding(paddingValues)
                .padding(8.dp)
                .padding(top = 16.dp)
                .fillMaxWidth(),
            verticalArrangement = Arrangement.spacedBy(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            OutlinedTextField(
                value = name, onValueChange = { name = it },
                modifier = Modifier
                    .fillMaxWidth(.8f),
                label = { Text("Name") },
                placeholder = { Text("Expense Name") },
                keyboardActions = KeyboardActions(onNext = {
                    focusManager.moveFocus(FocusDirection.Down)
                }),
                keyboardOptions = KeyboardOptions(
                    imeAction = ImeAction.Next,
                    capitalization = KeyboardCapitalization.Words
                )
            )
            CurrencyField(
                value = value,
                onValueChange = { value = it },
                modifier = Modifier
                    .fillMaxWidth(.8f),
                label = { Text("Value") },
                leadingIcon = { Text("RON") },
                keyboardActions = KeyboardActions(onDone = {
                    submit()
                }),
                fixCursorAtTheEnd = false,
            )
            Row(
                modifier = Modifier.fillMaxWidth(.8f),
                horizontalArrangement = Arrangement.spacedBy(8.dp)
            ) {
                // These will be replaced by a Material Date Picker when they become available
                DateField(
                    tempDay,
                    tempMonth,
                    tempYear,
                    onValueChange = { day, month, year ->
                        tempDay = day
                        tempMonth = month
                        tempYear = year
                    },
                    keyboardActions = KeyboardActions(
                        onDone = {
                            date = date
                                .withDayOfMonth(tempDay)
                                .withMonth(tempMonth)
                                .withYear(tempYear)
                            tempDay = date.dayOfMonth
                            tempMonth = date.monthValue
                            tempYear = date.year
                        }
                    ),
                    label = { Text("Expense Date") },
                    modifier = Modifier.fillMaxWidth(.6f),
                    fixCursorAtTheEnd = false,
                )
                TimeField(
                    date.hour,
                    date.minute,
                    onValueChange = { hour, minute ->
                        date = date
                            .withHour(hour)
                            .withMinute(minute)
                    },
                    label = { Text("Expense Time") },
                    fixCursorAtTheEnd = false,
                )
            }
            Text("Labels")
            // Grid For Selected
            MyHorizontalGrid(
                gridRows = MyGridRows.FixedRows(3),
                modifier = Modifier
                    .fillMaxWidth(.8f)
                    .horizontalScroll(scrollState)
            ) {
                (selectedLabels + labels.filter { label -> !selectedLabels.contains(label) })
                    .forEach { label ->
                        LabelSuggestionChip(label, onClick = { l ->
                            if (selectedLabels.contains(l))
                                selectedLabels.remove(l)
                            else
                                selectedLabels.add(l)
                        },
                            selected = selectedLabels.contains(label)
                        )
                    }
            }
        }
    }
}