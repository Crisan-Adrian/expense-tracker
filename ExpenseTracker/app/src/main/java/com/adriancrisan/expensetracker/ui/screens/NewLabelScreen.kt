package com.adriancrisan.expensetracker.ui.screens

import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.navigation.NavController
import com.adriancrisan.expensetracker.data.entities.Label
import com.adriancrisan.expensetracker.data.entities.LabelIcon
import com.adriancrisan.expensetracker.ui.components.LabelForm
import com.adriancrisan.expensetracker.ui.icons.MyIcons
import com.adriancrisan.expensetracker.ui.viewmodels.LabelsModel
import kotlinx.coroutines.launch


@Composable
fun NewLabelScreen(
    navController: NavController,
    labelsModel: LabelsModel,
    onAdd: suspend (Label) -> Unit,
) {
    var icons: List<LabelIcon> by remember { mutableStateOf(listOf()) }

    LaunchedEffect(labelsModel) {
        icons = MyIcons.getIconList()
    }
    val coroutineScope = rememberCoroutineScope()
    var label = Label(name = "")

    val addLabel: suspend () -> Unit = {
        onAdd(label)
    }

    LabelForm(
        label = label,
        title = { Text("Add a label") },
        actionButtonContent = { Text("Save") },
        icons = icons,
        onSubmit = { submittedLabel ->
            coroutineScope.launch {
                label = submittedLabel
                addLabel()
            }
            navController.popBackStack()
        },
        onCancel = { navController.popBackStack() }
    )
}
